﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour
{
    public float movingSpeed = 15;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
            this.transform.Translate(Vector3.forward * movingSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.A))
            this.transform.Translate(Vector3.left * movingSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.S))
            this.transform.Translate(Vector3.back * movingSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.D))
            this.transform.Translate(Vector3.right * movingSpeed * Time.deltaTime);
    }
}
